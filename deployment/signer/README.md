# Prerequisites

1. Installed hashicorp vault instance along with vault agent injector - used to store credentials required by kubernetes pods
2. vault should be properly configured to allow kubernetes authentication while fetching credentials
3. If acme http validation is used, please ensure that `http01-edit-in-place` is set to `false` in your `values.yaml` otherwise rewrite rule on ingress will conflict with the validation.

steps for vault configuration

1. execute shell of your vault pod `kubectl exec -it vault-0 -- /bin/sh`. In this case pod name is `vault-0`
2. login to vault using cmd `vault login`. You will need to provide token for auth
3. create secret engine `vault secrets enable -path=dev kv-v2` in this case name of the engine is `dev`
4. enable kubernetes interaction with vault `vault auth enable kubernetes`
5. add config for kubernetes `vault write auth/kubernetes/config  kubernetes_host="https://$KUBERNETES_PORT_443_TCP_ADDR:443"`
6. write policy in vault for fetching credentials by kubernetes
```
vault policy write dev-policy - <<EOF
path "dev/data/*" {
   capabilities = ["read"]
}
EOF 
```
in this example `dev-policy` is name of policy - it can be anything, and path
`dev/data/*` needs to relate existing secret engine declared in pt 3.

7. create role in vault that will bind policy with given service account name and service account namespace

```
vault write auth/kubernetes/role/gaiax-edc_role \
      bound_service_account_names=gaiax-edc-dev* \
      bound_service_account_namespaces=gaiax-edc-dev* \
      policies=dev_policy \
      ttl=24h
```

Explanation: `gaiax-edc_role` is a role name, it can be anything. `gaiax-edc-dev*` is a name for both service accounts
and kubernetes namespaces names of services account. In this case `*` wildcard was used so to use this role in each namespace
there should be kubernetes service account created with the name starting from `gaiax-edc-dev` additionally this service
account need to be placed in namespace with a name starting from  `gaiax-edc-dev`. If you require other namespace naming convention
then the role need to be modified with correct namespaces names. `dev-policy` is a policy name defined in pt 6.

IMPORTANT  
Steps 1-7 need to be executed only once , if given role, policy, already exists in vault, then there is no need of configuring them again.

# Installation steps

1. We need to provide environment variables that are used in deployment.yaml To do so we need to login to vault and add them.
   values should be provided for secret engine defined in prerequisite section.
   path for those secrets should be created according to following convention <naamepsace_name>-<container_name>
   as an example in dev cluster path `gaiax-edc-dev-signer-signer` was provided so `gaiax-edc-dev-signer` is namespace name
   and `signer` is pod name. Once path is provided we need to provide secrets.

```
{
  "ENGINE_PATH": "/opt/plugins/hashicorp-vault-provider.so",
  "HTTP_HOST": "",
  "HTTP_IDLE_TIMEOUT": "120s",
  "HTTP_PORT": "8080",
  "HTTP_READ_TIMEOUT": "10s",
  "HTTP_WRITE_TIMEOUT": "10s",
  "LOG_ENCODING": "json",
  "LOG_LEVEL": "debug",
  "VAULT_ADRESS": "<internal access to vault accordint to convention: http://<service_name>.<namespace_name_of_given_service>svc.cluster.local:<service_port> example: http://vault.gaiax-edc-dev-vault.svc.cluster.local:8200>",
  "VAULT_TOKEN": "<token value that get access to vault>"
}
```
2. go to vault and define new transit secret engine with path `transit/simpl` create encryption key `gaia-x-key1` with type `ed25519`
3. go to values.yaml and change `ingress.annotations.cert-manager.io/cluster-issuer` to `dev-prod`
change `ingress.frontendDomain`, `ingress.frontendTlsSecretName`
4. Modify values in deployment.yaml . change `vault.hashicorp.com/role` to role name defined in prerequisite section.
change `vault.hashicorp.com/agent-inject-secret-config.txt` to the path of your credentials defined in your vault instance, do the same inside `vault.hashicorp.com/agent-inject-template-config.txt`
5. To deploy application into the cluster run `helm install <release_name> .` from signer/helm folder  