To upload new pdf files:
- put them into the `docker` folder near Dockerfile
- change the name of the file you you copy in the Dockerfile, eg `COPY your document.pdf /usr/share/nginx/html/your document.pdf`
- rebuild the image:
`docker buildx build --platform linux/amd64 -t code.europa.eu:4567/simpl/simpl-open/development/gaia-x-edc/poc-gaia-edc/pdf-files .`
- push the new image `docker push code.europa.eu:4567/simpl/simpl-open/development/gaia-x-edc/poc-gaia-edc/pdf-files:latest`
- redeploy the helm chart in the `helm` folder

