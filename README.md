# About the project
This PoC is a backend registering the service offering.

Dependencies:
- Self-Description Tooling https://gitlab.eclipse.org/eclipse/xfsc/self-description-tooling. Used to store data from which verifiable credentials can be made
- XFSC signer service https://gitlab.eclipse.org/eclipse/xfsc/tsa/signer/-/tree/ocm-wstack. Used to make verifiable credentials / presentations out of service offering
- Federated catalogue https://gitlab.eclipse.org/eclipse/xfsc/cat. Used to make register the service offering for the future search.
## Owner
Szymon.Truszczynski@t-systems.com

Valerii.Kalashnikov@t-systems.com

## How to start the dev environment
Add keycloak host to your local hosts file:
Run `sudo nano /private/etc/hosts`
and add line
`127.0.0.1       key-server`

then
Run `./dev/dev-env-start.sh`

It will run the the dependent services in Docker containers.

after Keycloak service is up and running 

Run `./dev/kc-user-definition.sh`
this will create test user with proper permission in keycloak 

## How to stop the dev environment
Run `./dev/dev-env-stop.sh`

## How to run swagger-ui
- Run the application
- Go to http://localhost:8084/swagger-ui/index.html#/

## How to import and run e2e tests in postman
- to import collection go to postman: File -> Import -> Select file from postman-collection folder to import collection with tests
- to launch tests go to postman: navigate to poc-gaia-edc collection -> right click -> run collection -> click Run poc-gaia-edc button