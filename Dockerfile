FROM eclipse-temurin:21-jdk-alpine

COPY target/*.jar poc-0.0.1-SNAPSHOT.jar

RUN mkdir /files && chmod 775 /files
RUN adduser -u 8877 -D dockerUser
RUN chown -R dockerUser:dockerUser /files

USER dockerUser

EXPOSE 8084
ENTRYPOINT ["java", "-jar", "/poc-0.0.1-SNAPSHOT.jar"]