package eu.europa.ec.simpl.adapter;

import feign.FeignException;
import feign.FeignException.Unauthorized;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.resource.NoResourceFoundException;

@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(NoResourceFoundException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handleNoRecordFoundException(final NoResourceFoundException ex) {
        return "Resource Not Found";
    }

    @ExceptionHandler(Unauthorized.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ApiResponse(responseCode = "401", content = @Content(mediaType = "text/plain", schema = @Schema(type = "string",
            example = "Unauthorized or token expired")))
    public String handleUnauthorizedException(final FeignException ex) {
        log.error(ex.getMessage());
        return "Unauthorized or token expired\n";
    }

    @ExceptionHandler(FeignException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_GATEWAY)
    @ApiResponse(responseCode = "502", content = @Content(mediaType = "text/plain", schema = @Schema(type = "string",
            example = "server error\nConnection refused executing GET http://localhost:8080/getAvailableShapes")))
    public String handleFeignException(final FeignException ex) {
        log.error(ex.getMessage());
        return "server error\n";
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<String> handleIllegalArgumentException(final IllegalArgumentException e) {
        String json = String.format("{\"code\": %d, \"message\": \"%s\"}",
                                    HttpStatus.BAD_REQUEST.value(),
                                    e.getMessage().replace("\"", "\\\"")); // Properly escape JSON strings

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(json);
    }
}