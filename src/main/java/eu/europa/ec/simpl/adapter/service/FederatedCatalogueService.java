package eu.europa.ec.simpl.adapter.service;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.europa.ec.simpl.adapter.client.FederatedCatalogueClient;
import eu.europa.ec.simpl.adapter.query.QuickSearchQueryGenerator;
import eu.europa.ec.simpl.adapter.request.SearchRequest;
import lombok.RequiredArgsConstructor;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class FederatedCatalogueService {

    private final FederatedCatalogueClient federatedCatalogueClient;

    public String getQueryResultQuickSearch(final String bearerToken, final String searchString) {
        var queryGenerator = new QuickSearchQueryGenerator();
        if (isSearchStringEffectivelyEmpty(searchString)) {
            throw new IllegalArgumentException("Search string must not be empty");
        }

        var searchParams = searchString.split(",");

        for (String param : searchParams) {
            boolean isValid = param.matches("[a-zA-Z0-9]*");
            if (!isValid) {
                throw new IllegalArgumentException("Search string must not contain any special symbols");
            }
        }

        var query = queryGenerator.generateQuery(searchParams);

        Map<String, String> parameters = new HashMap<>();
        parameters.put(searchParams[0], searchParams[0]);


        final var searchRequest = SearchRequest.builder()
                .statement(query)
                .parameters(parameters)
                .build();
        final var jsonQuery = searchRequest.serializeToJson();
        var response = federatedCatalogueClient.executeQueryWithoutTotalCount(jsonQuery);
        ObjectMapper objectMapper = new ObjectMapper();
        RoleFilterService roleFilter = new RoleFilterService(objectMapper, federatedCatalogueClient);
        var filteredItems = roleFilter.applyRolesToItems(bearerToken, response);

        if (filteredItems.isEmpty()) {
            return """
                {
                    "totalCount": 0,
                    "items": []
                }
                """;
        }

        JSONObject result = new JSONObject();
        result.put("totalCount", filteredItems.size());
        result.put("items", new JSONArray(filteredItems));
        return result.toString();
    }

    private boolean isSearchStringEffectivelyEmpty(final String searchString) {
        return searchString == null || searchString.trim().isEmpty() || searchString.replaceAll(",$", "").isEmpty();
    }
}
