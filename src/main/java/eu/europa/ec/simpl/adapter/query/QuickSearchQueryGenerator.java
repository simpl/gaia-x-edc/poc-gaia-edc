package eu.europa.ec.simpl.adapter.query;

import java.util.Arrays;
import java.util.stream.Collectors;

public class QuickSearchQueryGenerator {

    private static final int MAX_SEARCH_PARAMS = 16;

    public String generateQuery(final String[] terms) {
        if (terms.length > MAX_SEARCH_PARAMS) {
            throw new IllegalArgumentException("The number of search parameters cannot exceed " + MAX_SEARCH_PARAMS);
        }
        // Build the dynamic part of the Cypher query to check if any 'value' contains any of the terms
        String searchTermCheck = Arrays.stream(terms)
                .map(term -> String.format("value =~ '(?i).*%1$s.*' OR size([x IN value WHERE x =~ '(?i).*%1$s.*']) > 0",
                        term.replace("'", "\\'")))
                .collect(Collectors.joining(" OR "));

        return String.format(
            "CALL apoc.meta.nodeTypeProperties() YIELD propertyName  " +
            "WITH collect(propertyName) AS properties  " +
            "MATCH (n)  " +
            "UNWIND properties AS prop  " +
            "WITH n, prop, n[prop] AS value  " +
            "WHERE value IS NOT NULL AND  " +
            "    (%s)  " +
            "WITH n,  " +
            "    (CASE WHEN 'GeneralServiceProperties' IN labels(n) THEN n ELSE NULL END) AS gspNode,  " +
            "    (CASE WHEN 'GeneralServiceProperties' IN labels(n) THEN NULL ELSE n END) AS nonGspNode  " +
            "OPTIONAL MATCH (nonGspNode)<--(p)-->(m:GeneralServiceProperties)  " +
            "WHERE nonGspNode IS NOT NULL AND m IS NOT NULL AND NOT m = nonGspNode  " +
            "WITH COALESCE(m, nonGspNode) AS matchedNode, gspNode  " +
            "WITH COLLECT(DISTINCT matchedNode) + COLLECT(DISTINCT gspNode) AS allNodes  " +
            "UNWIND allNodes AS i  " +
            "WITH DISTINCT i  " +
            "WITH collect(distinct i) AS allConnectedNodes " +
            "WITH [node IN allConnectedNodes WHERE 'GeneralServiceProperties' IN labels(node)] AS filteredNodes " +
            "WITH filteredNodes, size(filteredNodes) AS totalCount " +
            "UNWIND filteredNodes AS individualNode " +
            "RETURN individualNode AS i, totalCount",
            searchTermCheck
        );
    }
}
