package eu.europa.ec.simpl.adapter.controller;

import eu.europa.ec.simpl.adapter.service.AdvancedSearchService;
import eu.europa.ec.simpl.adapter.service.FederatedCatalogueService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import jakarta.validation.constraints.NotBlank;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class FederatedCatalogueProxyController {

    private static final String ADVANCED_SEARCH_EXAMPLE_REQUEST = """
            {
              "simpl:OfferingPrice": {
                "@type": "simpl:OfferingPrice",
                "price": {
                    "op1": ">=",
                    "value1": "15",
                    "op2": "<",
                    "value2": "150"
                }
              },
              "simpl:GeneralServiceProperties": {
                "@type": "simpl:GeneralServiceProperties",
                "name": "szym"
              },
              "simpl:ContractTemplate": {
                "@type": "simpl:ContractTemplate",
                "contractTemplateDocument": "Contract Template xxx"
              }
            }
            """;

    private static final String ADVANCED_SEARCH_EXAMPLE_RESPONSE = """
            {
                "totalCount": 1,
                "items": [
                    {
                        "n": {
                            "name": "szym",
                            "inLanguage": "en",
                            "description": "def",
                            "claimsGraphUri": [
                                "did:web:registry.gaia-x.eu:DataOffering:fMw2UtNCDW-ydI83YKscqCKa-n75jJ0qY000"
                            ],
                            "serviceAccessPoint": "https://creation.com"
                        }
                    }
                ]
            }
            """;

    private static final String QUICK_SEARCH_EXAMPLE_RESPONSE = """
            {
                "totalCount": 4,
                "items": [
                    {
                        "i": {
                            "claimsGraphUri": [
                                "did:web:registry.gaia-x.eu:DataOffering:fMw2UtNCDW-ydI83YKscqCKa-n75jJ0qY7v7===="
                            ],
                            "name": "aefaf",
                            "description": "afafa",
                            "inLanguage": "en",
                            "serviceAccessPoint": "https://creation.com"
                        }
                    },
                    {
                        "i": {
                            "claimsGraphUri": [
                                "did:web:registry.gaia-x.eu:DataOffering:fMw2UtNCDW-ydI83YKscqCKa-n75jJ0qY7v1"
                            ],
                            "name": "aefaf",
                            "description": "afafa",
                            "inLanguage": "en",
                            "serviceAccessPoint": "https://creation.com"
                        }
                    },
                    {
                        "i": {
                            "claimsGraphUri": [
                                "did:web:registry.gaia-x.eu:DataOffering:fMw2UtNCDW-ydI83YKscqCKa-n75jJ0qY7v0"
                            ],
                            "name": "aefaf",
                            "description": "afafa",
                            "inLanguage": "en",
                            "serviceAccessPoint": "https://creation.com"
                        }
                    },
                    {
                        "i": {
                            "claimsGraphUri": [
                                "did:web:registry.gaia-x.eu:DataOffering:fMw2UtNCDW-ydI83YKscqCKa-n75jJ0qY7v8===="
                            ],
                            "name": "aefaf",
                            "description": "afafa",
                            "inLanguage": "en",
                            "serviceAccessPoint": "https://creation.com"
                        }
                    }
                ]
            }
            """;

    //reason for this hardocded token is that quicksearch and andvancedsearch expects any jwt token to parse
    // we're currently in a situation where we do not have keycloak integration and (temporarily) no gateway deployed
    // in dev environment. Once gateway will be deployed this token will be removed
    private static final String BEARER_TOKEN_MOCK = "Bearer eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJE" +
            "S0xUNlhjdnE2b1h0bFZqekFQTk5sZkxSRzdjaHMyTklaUnlKaVlyOElnIn0.eyJleHAiOjE3MzIwOTc2MzksImlhdCI6MTczMjA" +
            "5NjczOSwianRpIjoiYWI0ZjQyNTUtMjJjNS00MDU5LWI1NDMtNzVkOWRlYzhkM2FkIiwiaXNzIjoiaHR0cHM6Ly9rZXktc2Vydm" +
            "VyLmRldi5zaW1wbC1ldXJvcGUuZXUvcmVhbG1zL2dhaWEteCIsInN1YiI6Ijc0MWRhODFiLTVhZmItNDVjZS1hZTA3LTM3YjViY" +
            "TIwOGY5NSIsInR5cCI6IkJlYXJlciIsImF6cCI6ImZlZGVyYXRlZC1jYXRhbG9ndWUiLCJzZXNzaW9uX3N0YXRlIjoiOTFiMDY0" +
            "NGItMjc4Ni00MWE1LTk4ZDctODg4ZGIxNGIyMjI4IiwiYWxsb3dlZC1vcmlnaW5zIjpbIioiXSwicmVhbG1fYWNjZXNzIjp7InJ" +
            "vbGVzIjpbImRlZmF1bHQtcm9sZXMtZ2FpYS14Iiwib2ZmbGluZV9hY2Nlc3MiLCJ1bWFfYXV0aG9yaXphdGlvbiJdfSwicmVzb3" +
            "VyY2VfYWNjZXNzIjp7ImZlZGVyYXRlZC1jYXRhbG9ndWUiOnsicm9sZXMiOlsiUm8tTVUtQ0EiLCJSby1NVS1BIiwiUm8tU0QtQ" +
            "SIsIlJvLVBBLUEiXX19LCJzY29wZSI6ImdhaWEteCIsInNpZCI6IjkxYjA2NDRiLTI3ODYtNDFhNS05OGQ3LTg4OGRiMTRiMjIy" +
            "OCIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJ1c2VyIiwiZ2l2ZW5fbmFtZSI6IiIsImZhbWl" +
            "seV9uYW1lIjoiIiwiZW1haWwiOiJ1c2VyQHVzZXIudXNlciIsImlkZW50aXR5X2F0dHJpYnV0ZXMiOlsiQ09OU1VNRVIiXX0.cP" +
            "t9INGFSHngdE_Uz6QEeRWjX7DvzjEbQdnXOxZicRbeVkyBsp9K06ByDXKzzjw1Rfgr1A49TtxM2BfDFOMBurFsb1e6e-FF4TY2f" +
            "eTNLISGAhvck258_WULl9P_YTo09cSP3N64F7iGPmb3k-yjPmwhn1Bozp6pnntGAJ2N8gmtqtMbk62HUSvfC32yr4fPuXVT5UOU" +
            "qLjXeIMmmay-4tJth7qEXW2WE-a9uSSnI19d7GEpwNq4eE1lthY1Ju944V-c4N1UzJ6j4T6VUCRKIH9xiIYzfsO-uyv4iKFUB8_" +
            "y3qk4SIvUIylijRFxnaXjObzCKPUiwXqUZlXyCZe5Cw";
    private final FederatedCatalogueService federatedCatalogueService;
    private final AdvancedSearchService advancedSearchService;

    @Operation(summary = "Query Federated catalogue")
    @ApiResponse(responseCode = "200", content = {@Content(mediaType = "application/json",
            schema = @Schema(type = "string", example = QUICK_SEARCH_EXAMPLE_RESPONSE))})
    @PostMapping(value = "/search/quick", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public String queryCatalogueQuickSearch(
        @Parameter(hidden = true) @RequestHeader(value = "Authorization", defaultValue = BEARER_TOKEN_MOCK)
        final String authToken,
        @RequestParam final String searchString) {
        return federatedCatalogueService
                .getQueryResultQuickSearch(authToken, searchString);
    }

    @Operation(summary = "Query Federated catalogue based on provided vocabulary")
    @ApiResponse(responseCode = "200", content = {@Content(mediaType = "application/json",
            schema = @Schema(type = "string", example = ADVANCED_SEARCH_EXAMPLE_RESPONSE))})
    @PostMapping(value = "/search/advanced", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public String queryCatalogueAdvancedSearch(
            @Parameter(hidden = true) @RequestHeader(value = "Authorization", defaultValue = BEARER_TOKEN_MOCK)
            final String authToken,
            @Schema(type = "string", example = ADVANCED_SEARCH_EXAMPLE_REQUEST) @RequestBody @NotBlank final String request) {
        return advancedSearchService.getQueryResultAdvancedSearch(authToken, request);
    }
}
