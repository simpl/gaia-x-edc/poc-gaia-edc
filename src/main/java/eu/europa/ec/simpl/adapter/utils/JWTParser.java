package eu.europa.ec.simpl.adapter.utils;

import java.util.List;

import com.auth0.jwt.JWT;

public class JWTParser {

    private JWTParser() {

    }

    public static List<String> extractIdentityAttributes(final String token) {
        return JWT.decode(token).getClaim("identity_attributes").asList(String.class);
    }
}
