package eu.europa.ec.simpl.adapter.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;

import static com.fasterxml.jackson.databind.node.JsonNodeType.OBJECT;

@Getter
@Setter
public class AdvancedSearchPropertiesParser {

    public static final String VALID_PARAMETER_REGEX = "[a-zA-Z0-9+><=,. :/@-]*";
    public static final String COMMA_SEPARATOR_STATEMENT = ",";

    private AdvancedSearchPropertiesParser() {
    }

    @Getter
    @Builder
    public static class SearchProperty {
        private String propertyName;
        private String propertyValue;
        private String nodeName;
    }

    @SneakyThrows
    public static List<SearchProperty> parseRequest(final String request) {
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode rootNode = objectMapper.readTree(request);

        List<SearchProperty> searchProperties = new ArrayList<>();

        // Iterate over each node (like simpl:DataProperties, simpl:GeneralServiceProperties, etc.)
        Iterator<Map.Entry<String, JsonNode>> fieldsIterator = rootNode.fields();
        while (fieldsIterator.hasNext()) {
            Map.Entry<String, JsonNode> field = fieldsIterator.next();
            String nodeName = field.getKey(); // e.g., simpl:DataProperties
            JsonNode nodeObject = field.getValue(); // the actual object value

            // Remove the 'simpl.' prefix from the nodeName
            String nodeNameWithoutPrefix = nodeName.split(":")[1];

            // Iterate over the fields inside each node object
            Iterator<Map.Entry<String, JsonNode>> nodeFieldsIterator = nodeObject.fields();
            while (nodeFieldsIterator.hasNext()) {
                Map.Entry<String, JsonNode> nodeField = nodeFieldsIterator.next();

                // Ignore fields like '@type'
                if (nodeField.getKey().equals("@type")) {
                    continue;
                }

                // special case for price with range of values
                if (nodeField.getValue().getNodeType().equals(OBJECT)) {
                    SearchProperty searchProperty = buildSearchProperty(nodeField.getKey(),
                            buildPropertyNameForRangeNumber(nodeField),
                            nodeNameWithoutPrefix);
                    searchProperties.add(searchProperty);

                } else {
                    // Build and add the SearchProperty
                    SearchProperty searchProperty = buildSearchProperty(nodeField.getKey(),
                            nodeField.getValue().asText(), nodeNameWithoutPrefix);
                    searchProperties.add(searchProperty);
                }
            }
        }
        return searchProperties;
    }

    private static SearchProperty buildSearchProperty(final String propertyName, final String propertyValue,
                                                      final String nodeName) {
        validateExtractedParameter(propertyValue);
        return SearchProperty.builder()
                .propertyName(propertyName)
                .propertyValue(propertyValue.trim())
                .nodeName(nodeName)
                .build();
    }

    private static String buildPropertyNameForRangeNumber(final Map.Entry<String, JsonNode> nodeField) {
        final var firstOperator = nodeField.getValue().findValue("op1").textValue();
        final var firstOperatorValue = nodeField.getValue().findValue("value1").textValue();

        return Objects.nonNull(nodeField.getValue().findValue("op2"))
                ? firstOperator + firstOperatorValue + COMMA_SEPARATOR_STATEMENT
                  + nodeField.getValue().findValue("op2").textValue()
                  + nodeField.getValue().findValue("value2").textValue()
                : firstOperator + firstOperatorValue;
    }

    public static void validateExtractedParameter(final String extractedParameter) {
        if (Objects.isNull(extractedParameter) || extractedParameter.isBlank()
                || !extractedParameter.matches(VALID_PARAMETER_REGEX)) {
            throw new IllegalArgumentException("Provided values for search must not be empty and contain "
                    + "only alphanumeric characters or +><=,. :/@-");
        }
    }
}
