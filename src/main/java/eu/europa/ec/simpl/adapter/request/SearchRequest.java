package eu.europa.ec.simpl.adapter.request;

import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Builder
@Slf4j
public class SearchRequest {
    private final String statement;
    private final Map<String,String> parameters;
    
    public String serializeToJson() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(this);
        } catch (final JsonProcessingException e) {
            log.error("Error during requests serrialisation", e);
            return "{}"; // Return empty JSON object in case of error
        }
    }
}


