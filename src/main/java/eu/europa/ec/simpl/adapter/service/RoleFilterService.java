package eu.europa.ec.simpl.adapter.service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.europa.ec.simpl.adapter.client.FederatedCatalogueClient;
import eu.europa.ec.simpl.adapter.request.SearchRequest;
import eu.europa.ec.simpl.adapter.utils.JWTParser;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;

@Slf4j
@RequiredArgsConstructor
public class RoleFilterService {
    public static final String POLICY_FIELD_NAME = "policy";
    public static final String NODE_PROPERTY_I = "i";
    public static final String NODE_PROPERTY_ITEMS = "items";

    private final ObjectMapper objectMapper;
    private final FederatedCatalogueClient federatedCatalogueClient;

    @SneakyThrows
    public List<JSONObject> applyRolesToItems(final String bearerToken, final String response) {
        final List<String> claimGraphUris = extractClaimGraphUrisFromResponse(response);
        if (claimGraphUris.isEmpty()) {
            return Collections.emptyList();
        }
        final var requestToFetchMatchedGeneralServiceProperties = SearchRequest.builder()
                .statement(produceNeo4jStatementForGetGeneralServicePropertiesWithServicePolicyByUri(claimGraphUris))
                .parameters(Map.of())
                .build();
        
        String bearerPrefix = "Bearer ";
        var roles = JWTParser.extractIdentityAttributes(bearerToken.substring(bearerPrefix.length()));

        var requestToFetchServicePropsJSON = requestToFetchMatchedGeneralServiceProperties.serializeToJson();
        var serviceProps = federatedCatalogueClient.executeQueryWithTotalCount(requestToFetchServicePropsJSON);
        return filterItemsByRole(serviceProps, roles.toArray(new String[0]));
        
    }

    @SneakyThrows
    private List<String> extractClaimGraphUrisFromResponse(final String jsonResponse) {
        JsonNode rootNode = objectMapper.readTree(jsonResponse);
        List<String> claimsGraphUris = new ArrayList<>();

        JsonNode items = rootNode.path(NODE_PROPERTY_ITEMS);

        for (JsonNode item : items) {
            // Use quickSearch if "i" is present; otherwise, use advancedSearch
            if (item.has(NODE_PROPERTY_I)) {
                claimsGraphUris.addAll(quickSearch(item));
            } else {
                claimsGraphUris.addAll(advancedSearch(item));
            }
        }

        return claimsGraphUris;
    }

    private List<String> quickSearch(final JsonNode item) {
        List<String> uris = new ArrayList<>();
        JsonNode claimsGraphUriNode = item.path("i").path("claimsGraphUri");
        
        if (claimsGraphUriNode.isArray()) {
            for (JsonNode uriNode : claimsGraphUriNode) {
                uris.add(uriNode.asText());
            }
        }

        return uris;
    }

    private List<String> advancedSearch(final JsonNode item) {
        List<String> urisInItemNode = new ArrayList<>();

        for (JsonNode claimsGraphUriNode : item) {
            String extractedUriAsString = claimsGraphUriNode.get(0).asText();
            urisInItemNode.add(extractedUriAsString);
        }

        Set<String> uniqueUrisInItemNode = new HashSet<>(urisInItemNode);

        if (uniqueUrisInItemNode.size() == 1) {
            return List.of(uniqueUrisInItemNode.iterator().next());
        }

        return new ArrayList<>();
    }

    private String produceNeo4jStatementForGetGeneralServicePropertiesWithServicePolicyByUri(final List<String> claimGraphUris) {
        // Convert list of URIs to a comma-separated string
        final var comaSeparatedUrisAsString = claimGraphUris.stream()
                .map(uri -> "'" + uri + "'")
                .collect(Collectors.joining(","));
    
        // Construct and return the full query string
        return String.format(
            "MATCH (n:GeneralServiceProperties) WHERE ANY(uri IN n.claimsGraphUri WHERE uri IN [%s]) " +
            "OPTIONAL MATCH (n)--(intermediateNode)--(sp:ServicePolicy) " +
            "RETURN n, sp AS policy ",
            comaSeparatedUrisAsString
        );
    }

    @SneakyThrows
    private List<JSONObject> filterItemsByRole(final String jsonString, final String[] roles) {
        JSONObject jsonObject = new JSONObject(jsonString);
        JSONArray items = jsonObject.getJSONArray(NODE_PROPERTY_ITEMS);

        List<JSONObject> filteredItems = new ArrayList<>();

        for (int i = 0; i < items.length(); i++) {
            JSONObject item = items.getJSONObject(i);

            if (containsAnyRole(item, roles)) {
                item.remove(POLICY_FIELD_NAME);
                filteredItems.add(item);
            }
        }

        return filteredItems;
    }

    @SneakyThrows
    private boolean containsAnyRole(final JSONObject item, final String[] roles) {
        for (String role : roles) {
            if (containsByRole(item, role)) {
                return true;
            }
        }
        return false;
    }

    @SneakyThrows
    private boolean containsByRole(final JSONObject item, final String role) {
        if (!item.has(POLICY_FIELD_NAME)) {
            return false;
        }

        JSONObject policy = item.getJSONObject(POLICY_FIELD_NAME);
        //sometimes in payload its defined as array and sometimes it's a  string
        final var accessPolicyAsArray = policy.optJSONArray("access-policy");
        final var accessPolicyAsString = policy.optString("access-policy");

        if (Objects.isNull(accessPolicyAsArray) && accessPolicyAsString.isEmpty()) {
            return false;
        }

        if (Objects.nonNull(accessPolicyAsArray)) {
            return IntStream.range(0, accessPolicyAsArray.length())
                    .mapToObj(accessPolicyAsArray::getString)
                    .anyMatch(accessPolicy -> isRoleInAccessPolicy(accessPolicy, role));
        }

        return isRoleInAccessPolicy(accessPolicyAsString, role);
    }

    @SneakyThrows
    private boolean isRoleInAccessPolicy(final String policyString, final String role) {
        try {
            JsonNode policyJsonNode = objectMapper.readTree(policyString);
            JsonNode permissions = policyJsonNode.get("permission");

            if (permissions != null && permissions.isArray()) {
                for (JsonNode permission : permissions) {
                    if (roleMatches(permission, role) && constraintsMatch(permission)) {
                        return true;
                    }
                }
            }

            return false;
        } catch (final Exception e) {
            log.error("the item has wrong structure", e);
            return false;
        }
    }

    private boolean roleMatches(final JsonNode permission, final String role) {
        JsonNode assignee = permission.get("assignee");

        return assignee != null && role.equals(assignee.get("uid").asText());
    }

    private boolean constraintsMatch(final JsonNode permission) {
        JsonNode constraints = permission.get("constraint");
        // constraints are optional, if we don't have any additional constraints return true
        if (constraints == null || constraints.isEmpty()) {
            return true;
        }

        if (!constraints.isArray()) {
            return false;
        }
        

        Instant startDate = extractConstraintDate(constraints, "http://www.w3.org/ns/odrl/2/gteq");
        Instant endDate = extractConstraintDate(constraints, "http://www.w3.org/ns/odrl/2/lteq");
        Instant currentTime = Instant.now();

        if (startDate != null && endDate != null) {
            return currentTime.isAfter(startDate) && currentTime.isBefore(endDate);
        } else if (startDate != null) {
            return currentTime.isAfter(startDate);
        } else if (endDate != null) {
            return currentTime.isBefore(endDate);
        }
        return false;
    }

    private Instant extractConstraintDate(final JsonNode constraints, final String operator) {
        for (JsonNode constraint : constraints) {
            String leftOperand = constraint.get("leftOperand").asText();
            String constraintOperator = constraint.get("operator").asText();

            if ("http://www.w3.org/ns/odrl/2/dateTime".equals(leftOperand) && operator.equals(constraintOperator)) {
                return Instant.parse(constraint.get("rightOperand").asText());
            }
        }
        return null;
    }
}
