package eu.europa.ec.simpl.adapter;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
@OpenAPIDefinition(info = @Info(title = "catalogue-adapter-gaia-edc", version = "v0"))
public class CatalogAdapterApplication {

    public static void main(final String[] args) {
        SpringApplication.run(CatalogAdapterApplication.class, args);
    }
}
