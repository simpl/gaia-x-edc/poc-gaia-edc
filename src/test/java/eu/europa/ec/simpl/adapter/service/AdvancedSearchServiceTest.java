package eu.europa.ec.simpl.adapter.service;

import java.time.Clock;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import eu.europa.ec.simpl.adapter.client.FederatedCatalogueClient;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AdvancedSearchServiceTest {

    @Mock
    private FederatedCatalogueClient federatedCatalogueClient;

    @Mock
    private Clock clock;

    @InjectMocks
    private AdvancedSearchService advancedSearchService;

    @Test
    void getQueryResultAdvancedSearchWithoutClaimGraphUris() {
        String authToken = "Bearer eyJhbGciOiJIUzI1NiJ9."
            + "eyJpZGVudGl0eV9hdHRyaWJ1dGVzIjpbIlJlc2VhcmNoZXIiXX0."
            + "Dzod05tXTd52oub3vtW_j6sw2MsIBe8RGyq2Sdl_Gtc";
        final var request = "{ \"simpl:DataProperties\": { \"property1\": \"value1\" } }";
        final var mockResponse = "{ \"items\": [] }";

        final var captor = ArgumentCaptor.forClass(String.class);

        when(federatedCatalogueClient.executeQueryWithTotalCount(captor.capture())).thenReturn(mockResponse);

        final var result = advancedSearchService.getQueryResultAdvancedSearch(authToken, request);

        String capturedRequest = captor.getValue();

        assertThat(capturedRequest)
                .contains("MATCH (n1:DataProperties) where n1.`property1` =~ '(?i).*value1.*' RETURN n1.claimsGraphUri");

        assertThat(result).contains("\"totalCount\":0");
    }

    @Test
    void getQueryResultAdvancedSearchWithExactNumericalValue() {
        String authToken = "Bearer eyJhbGciOiJIUzI1NiJ9."
            + "eyJpZGVudGl0eV9hdHRyaWJ1dGVzIjpbIlJlc2VhcmNoZXIiXX0."
            + "Dzod05tXTd52oub3vtW_j6sw2MsIBe8RGyq2Sdl_Gtc";
        final var request = "{ \"simpl:DataProperties\": { \"price\": \"100\" } }";
        final var mockResponse = "{ \"items\": [] }";

        when(federatedCatalogueClient.executeQueryWithTotalCount(anyString())).thenReturn(mockResponse);

        final var result = advancedSearchService.getQueryResultAdvancedSearch(authToken, request);

        final var captor = ArgumentCaptor.forClass(String.class);
        verify(federatedCatalogueClient).executeQueryWithTotalCount(captor.capture());
        final var capturedRequest = captor.getValue();

        assertThat(capturedRequest)
                .contains("MATCH (n1:DataProperties) where toFloat(n1.`price`) = 100 RETURN n1.claimsGraphUri");

        assertThat(result).contains("\"totalCount\":0");
    }

    @Test
    void testGetQueryResultAdvancedSearchWithClaimGraphUris() {
        String authToken = "Bearer eyJhbGciOiJIUzI1NiJ9."
            + "eyJpZGVudGl0eV9hdHRyaWJ1dGVzIjpbIkNPTlNVTUVSX0NPREUiXX0."
            + "AFisZcuQYT09SlzEDqpR4-BMh09ziZt2jRnvVTVPeRM";
        final var request = "{ \"simpl:DataProperties\": { \"property1\": \"value1\" } }";
        final var mockResponseForFirstQuery = """
            {
                "items": [
                    [["http://example.com/uri1"]],
                    [["http://example.com/uri2"]]
                ]
            }
            """;
        final var mockResponseForSecondQuery = """
            {
                "items": [
                    {
                        "n": {
                            "claimsGraphUri": ["http://example.com/uri1"]
                        },
                        "policy": {
                            "access-policy": [
                                "{\\"permission\\": [{\\"assignee\\": {\\"uid\\": \\"CONSUMER_CODE\\"}}]}"
                            ]
                        }
                    },
                    {
                        "n": {
                            "claimsGraphUri": ["http://example.com/uri2"]
                        },
                        "policy": {
                            "access-policy": [
                                "{\\"permission\\": [{\\"assignee\\": {\\"uid\\": \\"CONSUMER_CODE\\"}}]}"
                            ]
                        }
                    },
                    {
                        "n": {
                            "claimsGraphUri": ["http://example.com/uri2"]
                        },
                        "policy": {
                            "access-policy": [
                                "{\\"permission\\": [{\\"assignee\\": {\\"uid\\": \\"RESEARCHER_CODE\\"}}]}"
                            ]
                        }
                    }
                ]
            }
            """;
    
        when(federatedCatalogueClient.executeQueryWithTotalCount(anyString()))
                .thenReturn(mockResponseForFirstQuery)   // First call returns claimGraphUris
                .thenReturn(mockResponseForSecondQuery); // Second call returns filtered service properties
    
        final var result = advancedSearchService.getQueryResultAdvancedSearch(authToken, request);
        final var captor = ArgumentCaptor.forClass(String.class);
        verify(federatedCatalogueClient, times(2)).executeQueryWithTotalCount(captor.capture());
        final var capturedRequests = captor.getAllValues();
    
        assertThat(capturedRequests.get(0))
                .contains("MATCH (n1:DataProperties) where n1.`property1` =~ '(?i).*value1.*' RETURN n1.claimsGraphUri");
    
        assertThat(capturedRequests.get(1))
                .contains("MATCH (n:GeneralServiceProperties) WHERE ANY(uri IN n.claimsGraphUri WHERE uri IN "
                          + "['http://example.com/uri1','http://example.com/uri2']) "
                          + "OPTIONAL MATCH (n)--(intermediateNode)--(sp:ServicePolicy) RETURN n, sp AS policy");
    
        assertThat(result).contains("\"totalCount\":2");
    }

    @Test
    void testGetQueryResultAdvancedSearchWithClaimGraphUrisWithMultipleRoles() {
        String authToken = "Bearer eyJhbGciOiJIUzI1NiJ9."
            + "eyJpZGVudGl0eV9hdHRyaWJ1dGVzIjpbIlJFU0VBUkNIRVJfQ09ERSIsIkNPTlNVTUVSX0NPREUiXX0."
            + "mHduNaETjvYKTwK1vX4NCZ2qVB4gH-8fN9aFRbj8w4E";
        final var request = "{ \"simpl:DataProperties\": { \"property1\": \"value1\" } }";
        final var mockResponseForFirstQuery = """
            {
                "items": [
                    [["http://example.com/uri1"]],
                    [["http://example.com/uri2"]]
                ]
            }
            """;
        final var mockResponseForSecondQuery = """
            {
                "items": [
                    {
                        "n": {
                            "claimsGraphUri": ["http://example.com/uri1"]
                        },
                        "policy": {
                            "access-policy": [
                                "{\\"permission\\": [{\\"assignee\\": {\\"uid\\": \\"CONSUMER_CODE\\"}}]}"
                            ]
                        }
                    },
                    {
                        "n": {
                            "claimsGraphUri": ["http://example.com/uri2"]
                        },
                        "policy": {
                            "access-policy": [
                                "{\\"permission\\": [{\\"assignee\\": {\\"uid\\": \\"CONSUMER_CODE\\"}}]}"
                            ]
                        }
                    },
                    {
                        "n": {
                            "claimsGraphUri": ["http://example.com/uri2"]
                        },
                        "policy": {
                            "access-policy": [
                                "{\\"permission\\": [{\\"assignee\\": {\\"uid\\": \\"RESEARCHER_CODE\\"}}]}"
                            ]
                        }
                    }
                ]
            }
            """;
    
        when(federatedCatalogueClient.executeQueryWithTotalCount(anyString()))
                .thenReturn(mockResponseForFirstQuery)   // First call returns claimGraphUris
                .thenReturn(mockResponseForSecondQuery); // Second call returns filtered service properties
    
        final var result = advancedSearchService.getQueryResultAdvancedSearch(authToken, request);
        final var captor = ArgumentCaptor.forClass(String.class);
        verify(federatedCatalogueClient, times(2)).executeQueryWithTotalCount(captor.capture());
        final var capturedRequests = captor.getAllValues();
    
        assertThat(capturedRequests.get(0))
                .contains("MATCH (n1:DataProperties) where n1.`property1` =~ '(?i).*value1.*' RETURN n1.claimsGraphUri");
    
        assertThat(capturedRequests.get(1))
                .contains("MATCH (n:GeneralServiceProperties) WHERE ANY(uri IN n.claimsGraphUri WHERE uri IN "
                          + "['http://example.com/uri1','http://example.com/uri2']) "
                          + "OPTIONAL MATCH (n)--(intermediateNode)--(sp:ServicePolicy) RETURN n, sp AS policy");
    
        assertThat(result).contains("\"totalCount\":3");
    }

    @Test
    void testGetQueryResultAdvancedSearchWithClaimGraphUrisWithConstraints() {
        String authToken = "Bearer eyJhbGciOiJIUzI1NiJ9."
            + "eyJpZGVudGl0eV9hdHRyaWJ1dGVzIjpbIlJFU0VBUkNIRVJfQ09ERSIsIkNPTlNVTUVSX0NPREUiXX0."
            + "mHduNaETjvYKTwK1vX4NCZ2qVB4gH-8fN9aFRbj8w4E";
        String timestampNow = getISOTimestampWithOffset(0, ChronoUnit.HOURS);
        String timestampPlus1Hour = getISOTimestampWithOffset(1, ChronoUnit.HOURS);
        final var request = "{ \"simpl:DataProperties\": { \"property1\": \"value1\" } }";
        final var mockResponseForFirstQuery = """
            {
                "items": [
                    [["http://example.com/uri1"]],
                    [["http://example.com/uri2"]]
                ]
            }
            """;
            
        final var mockResponseForSecondQuery = String.format("""
            {
                "items": [
                    {
                        "n": {
                            "claimsGraphUri": ["http://example.com/uri1"]
                        },
                        "policy": {
                            "access-policy": [
                                
                                "{\\"profile\\":\\"http://www.w3.org/ns/odrl/2/odrl.jsonld\\",\\"target\\":\\"bkPjUSXxXEBEDfLyxWpIYTSVvg8XYm37ufNk\\",\\"assigner\\":{\\"uid\\":\\"participant_id_0001\\",\\"role\\":\\"http://www.w3.org/ns/odrl/2/assigner\\"},\\"uid\\":\\"2743858b-1cfd-44d3-87cf-76694f763656\\",\\"@context\\":\\"http://www.w3.org/ns/odrl.jsonld\\",\\"@type\\":\\"Set\\",\\"permission\\":[{\\"target\\":\\"bkPjUSXxXEBEDfLyxWpIYTSVvg8XYm37ufNk\\",\\"assignee\\":{\\"uid\\":\\"RESEARCHER_CODE\\",\\"role\\":\\"http://www.w3.org/ns/odrl/2/assignee\\"},\\"action\\":[\\"http://simpl.eu/odrl/actions/search\\"],\\"constraint\\":[{\\"leftOperand\\":\\"http://www.w3.org/ns/odrl/2/dateTime\\",\\"operator\\":\\"http://www.w3.org/ns/odrl/2/gteq\\",\\"rightOperand\\":\\"%s\\"},{\\"leftOperand\\":\\"http://www.w3.org/ns/odrl/2/dateTime\\",\\"operator\\":\\"http://www.w3.org/ns/odrl/2/lteq\\",\\"rightOperand\\":\\"%s\\"}]}]}"
                            ]
                        }
                    }
                ]
            }
            """, timestampNow, timestampPlus1Hour);
    
        when(federatedCatalogueClient.executeQueryWithTotalCount(anyString()))
                .thenReturn(mockResponseForFirstQuery)   // First call returns claimGraphUris
                .thenReturn(mockResponseForSecondQuery); // Second call returns filtered service properties

        final var result = advancedSearchService.getQueryResultAdvancedSearch(authToken, request);
    
        assertThat(result).contains("\"totalCount\":1");
    }

    @Test
    void getQueryResultAdvancedSearchWithRangeNumericalValue() {
        String authToken = "Bearer eyJhbGciOiJIUzI1NiJ9."
            + "eyJpZGVudGl0eV9hdHRyaWJ1dGVzIjpbIlJlc2VhcmNoZXIiXX0."
            + "Dzod05tXTd52oub3vtW_j6sw2MsIBe8RGyq2Sdl_Gtc";
        final var request = "{ \"simpl:DataProperties\": { \"price\": \"<=50,>=20\" } }";
        final var mockResponse = "{ \"items\": [] }"; // Mock response with no URIs

        when(federatedCatalogueClient.executeQueryWithTotalCount(anyString())).thenReturn(mockResponse);

        final var result = advancedSearchService.getQueryResultAdvancedSearch(authToken, request);

        final var captor = ArgumentCaptor.forClass(String.class);
        verify(federatedCatalogueClient).executeQueryWithTotalCount(captor.capture());
        final var capturedRequest = captor.getValue();

        assertThat(capturedRequest)
                .contains("MATCH (n1:DataProperties) where toFloat(n1.`price`)<=50 and toFloat(n1.`price`)>=20 RETURN n1.claimsGraphUri");

        assertThat(result).contains("\"totalCount\":0");
    }

    public String getISOTimestampWithOffset(final long amountToAdd, final ChronoUnit unit) {
        // Start with the current time in UTC
        ZonedDateTime currentTime = ZonedDateTime.now(java.time.ZoneOffset.UTC);

        // Apply the offset if specified
        ZonedDateTime adjustedTime = currentTime.plus(amountToAdd, unit);
        
        // Format it to ISO 8601 format (e.g., 2024-10-31T10:00:00Z)
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssX");
        return adjustedTime.format(formatter);
    }
}