package eu.europa.ec.simpl.adapter;

import java.util.HashMap;

import feign.FeignException;
import feign.Request;
import feign.RequestTemplate;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpMethod;
import org.springframework.web.servlet.resource.NoResourceFoundException;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class GlobalExceptionHandlerTest {

    @InjectMocks
    private GlobalExceptionHandler globalExceptionHandler;

    @Test
    void handleNoRecordFoundExceptionShouldReturnExpectedResponse() {
        final var ex = new NoResourceFoundException(HttpMethod.GET, "/");

        final var result  = globalExceptionHandler.handleNoRecordFoundException(ex);

        assertThat(result).isEqualTo("Resource Not Found");
    }

    @Test
    void handleFeignExceptionShouldReturnExpectedResponse() {
        final var request = Request.create(Request.HttpMethod.GET, "url",
                new HashMap<>(), null, new RequestTemplate());
        final var ex = new FeignException.NotFound("", request, null, null);

        final var result = globalExceptionHandler.handleFeignException(ex);

        assertThat(result).containsIgnoringCase("server error");
    }

    @Test
    void handleIllegalArgumentExceptionShouldReturnExpectedResponse() {
        final var ex = new IllegalArgumentException("The number of search parameters cannot exceed");

        final var result = globalExceptionHandler.handleIllegalArgumentException(ex);

        assertThat(result.toString()).contains("Bad Request");
    }
}