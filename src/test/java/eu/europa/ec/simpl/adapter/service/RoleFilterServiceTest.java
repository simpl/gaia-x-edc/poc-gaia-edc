package eu.europa.ec.simpl.adapter.service;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.europa.ec.simpl.adapter.client.FederatedCatalogueClient;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RoleFilterServiceTest {

    private final ObjectMapper objectMapper = new ObjectMapper();

    private final String malformedServicePropsWrongAccessPolicyStructureArray = """
            {
                "items": [
                    {
                        "n": {
                            "claimsGraphUri": ["http://example.com/uri1"]
                        },
                        "policy": {
                            "access-policy": [
                                "something"
                            ]
                        }
                    }
                ]
            }
            """;

    private final String malformedServicePropsWrongAccessPolicyStructure = """
            {
                "items": [
                    {
                        "n": {
                            "claimsGraphUri": ["http://example.com/uri1"]
                        },
                        "policy": {
                            "access-policy": "something"
                        }
                    }
                ]
            }
            """;

    private final String malformedServicePropsMissingPolicyObject = """
            {
                "items": [
                    {
                        "n": {
                            "claimsGraphUri": ["http://example.com/uri1"]
                        }
                    }
                ]
            }
            """;

    @Mock
    private FederatedCatalogueClient federatedCatalogueClient;

    private RoleFilterService roleFilterService;

    @BeforeEach
    void setUp() {
        roleFilterService = new RoleFilterService(objectMapper, federatedCatalogueClient);
    }

    @ParameterizedTest
    @ValueSource(strings = {malformedServicePropsMissingPolicyObject,
                            malformedServicePropsWrongAccessPolicyStructureArray,
                            malformedServicePropsWrongAccessPolicyStructure})
    void testApplyRolesToItemsWithInvalidPolicyStructure(final String malformedJson) {
        String bearerToken = "Bearer eyJhbGciOiJIUzI1NiJ9."
                + "eyJpZGVudGl0eV9hdHRyaWJ1dGVzIjpbIlJFU0VBUkNIRVJfQ09ERSIsIkNPTlNVTUVSX0NPREUiXX0."
                + "mHduNaETjvYKTwK1vX4NCZ2qVB4gH-8fN9aFRbj8w4E";

        String validResponse = """
                {
                    "items": [
                        [["http://example.com/uri1"]],
                        [["http://example.com/uri2"]]
                    ]
                }
                """;

        when(federatedCatalogueClient.executeQueryWithTotalCount(anyString()))
                .thenReturn(malformedJson);

        List<JSONObject> result = roleFilterService.applyRolesToItems(bearerToken, validResponse);
        assertTrue(result.isEmpty());
    }

    @Test
    void testApplyRolesCheckDateConstraints() {
        String bearerToken = "Bearer eyJhbGciOiJIUzI1NiJ9."
                + "eyJpZGVudGl0eV9hdHRyaWJ1dGVzIjpbIlJFU0VBUkNIRVJfQ09ERSIsIkNPTlNVTUVSX0NPREUiXX0."
                + "mHduNaETjvYKTwK1vX4NCZ2qVB4gH-8fN9aFRbj8w4E";
        String timestampNow = getISOTimestampWithOffset(0, ChronoUnit.HOURS);
        String timestampPlus1Hour = getISOTimestampWithOffset(1, ChronoUnit.HOURS);

        String validResponse = """
                {
                    "items": [
                        [["http://example.com/uri1"]],
                        [["http://example.com/uri2"]]
                    ]
                }
                """;

        final var malformedServiceProps = String.format("""
                {
                    "items": [
                        {
                            "n": {
                                "claimsGraphUri": ["http://example.com/uri1"]
                            },
                            "policy": {
                                "access-policy": [
                                    
                                    "{\\"profile\\":\\"http://www.w3.org/ns/odrl/2/odrl.jsonld\\",\\"target\\":\\"bkPjUSXxXEBEDfLyxWpIYTSVvg8XYm37ufNk\\",\\"assigner\\":{\\"uid\\":\\"participant_id_0001\\",\\"role\\":\\"http://www.w3.org/ns/odrl/2/assigner\\"},\\"uid\\":\\"2743858b-1cfd-44d3-87cf-76694f763656\\",\\"@context\\":\\"http://www.w3.org/ns/odrl.jsonld\\",\\"@type\\":\\"Set\\",\\"permission\\":[{\\"target\\":\\"bkPjUSXxXEBEDfLyxWpIYTSVvg8XYm37ufNk\\",\\"assignee\\":{\\"uid\\":\\"RESEARCHER_CODE\\",\\"role\\":\\"http://www.w3.org/ns/odrl/2/assignee\\"},\\"action\\":[\\"http://simpl.eu/odrl/actions/search\\"],\\"constraint\\":[{\\"leftOperand\\":\\"http://www.w3.org/ns/odrl/2/dateTime\\",\\"operator\\":\\"http://www.w3.org/ns/odrl/2/gteq\\",\\"rightOperand\\":\\"%s\\"},{\\"leftOperand\\":\\"http://www.w3.org/ns/odrl/2/dateTime\\",\\"operator\\":\\"http://www.w3.org/ns/odrl/2/lteq\\",\\"rightOperand\\":\\"%s\\"}]}]}"
                                ]
                            }
                        }
                    ]
                }
                """, timestampNow, timestampPlus1Hour);

        when(federatedCatalogueClient.executeQueryWithTotalCount(anyString()))
                .thenReturn(malformedServiceProps);

        List<JSONObject> result = roleFilterService.applyRolesToItems(bearerToken, validResponse);
        assertEquals(1, result.size());
    }

    @Test
    void testApplyRolesCheckNoDateConstraints() {
        String bearerToken = "Bearer eyJhbGciOiJIUzI1NiJ9."
                + "eyJpZGVudGl0eV9hdHRyaWJ1dGVzIjpbIlJFU0VBUkNIRVJfQ09ERSIsIkNPTlNVTUVSX0NPREUiXX0."
                + "mHduNaETjvYKTwK1vX4NCZ2qVB4gH-8fN9aFRbj8w4E";

        String validResponse = """
                {
                    "items": [
                        [["http://example.com/uri1"]],
                        [["http://example.com/uri2"]]
                    ]
                }
                """;

        final var malformedServiceProps = """
                {
                    "items": [
                        {
                            "n": {
                                "claimsGraphUri": ["http://example.com/uri1"]
                            },
                            "policy": {
                                "access-policy": [
                                    "{\\"profile\\":\\"http://www.w3.org/ns/odrl/2/odrl.jsonld\\",\\"target\\":\\"bkPjUSXxXEBEDfLyxWpIYTSVvg8XYm37ufNk\\",\\"assigner\\":{\\"uid\\":\\"participant_id_0001\\",\\"role\\":\\"http://www.w3.org/ns/odrl/2/assigner\\"},\\"uid\\":\\"2743858b-1cfd-44d3-87cf-76694f763656\\",\\"@context\\":\\"http://www.w3.org/ns/odrl.jsonld\\",\\"@type\\":\\"Set\\",\\"permission\\":[{\\"target\\":\\"bkPjUSXxXEBEDfLyxWpIYTSVvg8XYm37ufNk\\",\\"assignee\\":{\\"uid\\":\\"RESEARCHER_CODE\\",\\"role\\":\\"http://www.w3.org/ns/odrl/2/assignee\\"},\\"action\\":[\\"http://simpl.eu/odrl/actions/search\\"],\\"constraint\\":[]}]}"
                                ]
                            }
                        }
                    ]
                }
                """;

        when(federatedCatalogueClient.executeQueryWithTotalCount(anyString()))
                .thenReturn(malformedServiceProps);

        List<JSONObject> result = roleFilterService.applyRolesToItems(bearerToken, validResponse);
        assertEquals(1, result.size());
    }

    @Test
    void testApplyRolesCheckEndDateOnly() {
        String bearerToken = "Bearer eyJhbGciOiJIUzI1NiJ9."
                + "eyJpZGVudGl0eV9hdHRyaWJ1dGVzIjpbIlJFU0VBUkNIRVJfQ09ERSIsIkNPTlNVTUVSX0NPREUiXX0."
                + "mHduNaETjvYKTwK1vX4NCZ2qVB4gH-8fN9aFRbj8w4E";

        String validResponse = """
                {
                    "items": [
                        [["http://example.com/uri1"]],
                        [["http://example.com/uri2"]]
                    ]
                }
                """;

        String timestampPlus1Hour = getISOTimestampWithOffset(1, ChronoUnit.HOURS);

        final var malformedServiceProps = String.format("""
                {
                    "items": [
                        {
                            "n": {
                                "claimsGraphUri": ["http://example.com/uri1"]
                            },
                            "policy": {
                                "access-policy": [
                                    "{\\"profile\\":\\"http://www.w3.org/ns/odrl/2/odrl.jsonld\\",\\"target\\":\\"bkPjUSXxXEBEDfLyxWpIYTSVvg8XYm37ufNk\\",\\"assigner\\":{\\"uid\\":\\"participant_id_0001\\",\\"role\\":\\"http://www.w3.org/ns/odrl/2/assigner\\"},\\"uid\\":\\"2743858b-1cfd-44d3-87cf-76694f763656\\",\\"@context\\":\\"http://www.w3.org/ns/odrl.jsonld\\",\\"@type\\":\\"Set\\",\\"permission\\":[{\\"target\\":\\"bkPjUSXxXEBEDfLyxWpIYTSVvg8XYm37ufNk\\",\\"assignee\\":{\\"uid\\":\\"RESEARCHER_CODE\\",\\"role\\":\\"http://www.w3.org/ns/odrl/2/assignee\\"},\\"action\\":[\\"http://simpl.eu/odrl/actions/search\\"],\\"constraint\\":[{\\"leftOperand\\":\\"http://www.w3.org/ns/odrl/2/dateTime\\",\\"operator\\":\\"http://www.w3.org/ns/odrl/2/lteq\\",\\"rightOperand\\":\\"%s\\"}]}]}"
                                ]
                            }
                        }
                    ]
                }
                """, timestampPlus1Hour);

        when(federatedCatalogueClient.executeQueryWithTotalCount(anyString()))
                .thenReturn(malformedServiceProps);

        List<JSONObject> result = roleFilterService.applyRolesToItems(bearerToken, validResponse);
        assertEquals(1, result.size());
    }

    @Test
    void testApplyRolesCheckStartDateOnly() {
        String bearerToken = "Bearer eyJhbGciOiJIUzI1NiJ9."
                + "eyJpZGVudGl0eV9hdHRyaWJ1dGVzIjpbIlJFU0VBUkNIRVJfQ09ERSIsIkNPTlNVTUVSX0NPREUiXX0."
                + "mHduNaETjvYKTwK1vX4NCZ2qVB4gH-8fN9aFRbj8w4E";

        String validResponse = """
                {
                    "items": [
                        [["http://example.com/uri1"]],
                        [["http://example.com/uri2"]]
                    ]
                }
                """;

        String timestampMinus1Hour = getISOTimestampWithOffset(-1, ChronoUnit.HOURS);

        final var malformedServiceProps = String.format("""
                {
                    "items": [
                        {
                            "n": {
                                "claimsGraphUri": ["http://example.com/uri1"]
                            },
                            "policy": {
                                "access-policy": [
                                    "{\\"profile\\":\\"http://www.w3.org/ns/odrl/2/odrl.jsonld\\",\\"target\\":\\"bkPjUSXxXEBEDfLyxWpIYTSVvg8XYm37ufNk\\",\\"assigner\\":{\\"uid\\":\\"participant_id_0001\\",\\"role\\":\\"http://www.w3.org/ns/odrl/2/assigner\\"},\\"uid\\":\\"2743858b-1cfd-44d3-87cf-76694f763656\\",\\"@context\\":\\"http://www.w3.org/ns/odrl.jsonld\\",\\"@type\\":\\"Set\\",\\"permission\\":[{\\"target\\":\\"bkPjUSXxXEBEDfLyxWpIYTSVvg8XYm37ufNk\\",\\"assignee\\":{\\"uid\\":\\"RESEARCHER_CODE\\",\\"role\\":\\"http://www.w3.org/ns/odrl/2/assignee\\"},\\"action\\":[\\"http://simpl.eu/odrl/actions/search\\"],\\"constraint\\":[{\\"leftOperand\\":\\"http://www.w3.org/ns/odrl/2/dateTime\\",\\"operator\\":\\"http://www.w3.org/ns/odrl/2/gteq\\",\\"rightOperand\\":\\"%s\\"}]}]}"
                                ]
                            }
                        }
                    ]
                }
                """, timestampMinus1Hour);

        when(federatedCatalogueClient.executeQueryWithTotalCount(anyString()))
                .thenReturn(malformedServiceProps);

        List<JSONObject> result = roleFilterService.applyRolesToItems(bearerToken, validResponse);
        assertEquals(1, result.size());
    }

    public String getISOTimestampWithOffset(final long amountToAdd, final ChronoUnit unit) {
        // Start with the current time in UTC
        ZonedDateTime currentTime = ZonedDateTime.now(java.time.ZoneOffset.UTC);

        // Apply the offset if specified
        ZonedDateTime adjustedTime = currentTime.plus(amountToAdd, unit);

        // Format it to ISO 8601 format (e.g., 2024-10-31T10:00:00Z)
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssX");
        return adjustedTime.format(formatter);
    }
}