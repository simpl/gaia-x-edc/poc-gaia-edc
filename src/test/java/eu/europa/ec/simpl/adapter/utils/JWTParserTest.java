package eu.europa.ec.simpl.adapter.utils;

import java.util.List;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class JWTParserTest {

    // valid JWT token with the "identity_attributes" field
    private static final String JWT_WITH_ATTRIBUTES = 
        "eyJhbGciOiJIUzI1NiJ9."
        + "eyJpZGVudGl0eV9hdHRyaWJ1dGVzIjpbIlJlc2VhcmNoZXIiXX0."
        + "Dzod05tXTd52oub3vtW_j6sw2MsIBe8RGyq2Sdl_Gtc";

    @Test
    void testExtractIdentityAttributes_Success() {
        List<String> identityAttributes;
        identityAttributes = JWTParser.extractIdentityAttributes(JWT_WITH_ATTRIBUTES);

        assertNotNull(identityAttributes);
        assertEquals("Researcher", identityAttributes.get(0));
    }
}