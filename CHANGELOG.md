## 1.0.4-SNAPSHOT.588.ab00a49e (2025-03-13)

No changes.


| author name         | ticket url                                        | sprint number | comment                    | action: ADDED/UPDATED/REMOVED |
|---------------------|---------------------------------------------------|---------------|----------------------------|-------------------------------|
| Valerii Kalashnikov | https://jira.simplprogramme.eu/browse/SIMPL-5988  | 9             |                            |                               |
| Valerii Kalashnikov | https://jira.simplprogramme.eu/browse/SIMPL-5988  | 9             |                            |                               |
| Valerii Kalashnikov | https://jira.simplprogramme.eu/browse/SIMPL-5987  | 9             |                            |                               |
| Valerii Kalashnikov | https://jira.simplprogramme.eu/browse/SIMPL-5244  | 9             |                            |                               |
| Szymon Truszczyński | https://jira.simplprogramme.eu/browse/SIMPL-8195  | 9             |                            |                               |
| Szymon Truszczyński | https://jira.simplprogramme.eu/browse/SIMPL-7728  | 9             | already released in 0.1.10 |                               |
| Szymon Truszczyński | https://jira.simplprogramme.eu/browse/SIMPL-5246  | 8             |                            |                               |
| Szymon Truszczyński | https://jira.simplprogramme.eu/browse/SIMPL-6348  | 8             |                            |                               |
| Szymon Truszczyński | https://jira.simplprogramme.eu/browse/SIMPL-6076  | 8             |                            |                               |
| Szymon Truszczyński | https://jira.simplprogramme.eu/browse/SIMPL-6946  | 8             |                            |                               |
| Valerii Kalashnikov | https://jira.simplprogramme.eu/browse/SIMPL-1251  | 8             |                            |                               |
| Szymon Truszczyński | https://jira.simplprogramme.eu/browse/SIMPL-5286  | 7             |                            |                               |
| Szymon Truszczyński | https://jira.simplprogramme.eu/browse/SIMPL-5528  | 7             |                            |                               |
| Valerii Kalashnikov | https://jira.simplprogramme.eu/browse/SIMPL-5033  | 10            |                            |                               |
| Szymon Truszczyński | https://jira.simplprogramme.eu/browse/SIMPL-6702  | 10            |                            |                               |
| Szymon Truszczyński | https://jira.simplprogramme.eu/browse/SIMPL-9147  | 10            |                            |                               |
| Valerii Kalashnikov | https://jira.simplprogramme.eu/browse/SIMPL-7956  | 12            |                            |                               |
| Szymon Truszczyński | https://jira.simplprogramme.eu/browse/SIMPL-9894  | 11            |                            |                               |
| Szymon Truszczyński | https://jira.simplprogramme.eu/browse/SIMPL-8146  | 11            |                            |                               |
| Szymon Truszczyński | https://jira.simplprogramme.eu/browse/SIMPL-10144 | 12            |                            |                               |
| Valerii Kalashnikov | https://jira.simplprogramme.eu/browse/SIMPL-8487  | 12            |                            |                               |
| Szymon Truszczyński | https://jira.simplprogramme.eu/browse/SIMPL-10148 | 12            |                            | ADDED                         |
| Daniel Witkowski    | https://jira.simplprogramme.eu/browse/SIMPL-10917 | 12            |                            | UPDATED                       |
